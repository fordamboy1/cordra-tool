import fs from 'fs';
import path from 'path';

export function findPackageRoot(): string {
    let res = process.cwd();
    while (true) {
        if (fs.existsSync(path.join(res, 'package.json'))) return res;
        res = path.dirname(res);
        if (res === path.dirname(res)) return res;
    }
}
