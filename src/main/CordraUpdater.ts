import { Blob, CordraClient, CordraObject } from "@cnri/cordra-client";
import path from "path";
import fs from "fs";
import PromisePool from 'es6-promise-pool';
import defaultDesign from './defaults/design.json';
import defaultTypes from './defaults/types.json';

interface DesignBits {
    handleMintingConfig?: {
        javascript?: string;
    };
    builtInTypes?: {
        CordraDesign?: {
            javascript?: string;
        };
        Schema?: {
            javascript?: string;
        };
    };
    javascript?: string;
}

export class CordraUpdater {
    private client: CordraClient;
    private readonly rootDir: string;

    constructor(client: CordraClient, root = __dirname) {
        this.client = client;
        this.rootDir = root;
    }

    async deleteObjectsIfNeeded(deleteAll: boolean, deleteAllIncludingTypes: boolean): Promise<void> {
        if (!deleteAll && !deleteAllIncludingTypes) return;
        console.log('Deleting non-Type objects');
        const objects = (await this.client.searchHandles('*:* -type:Schema')).results;
        for (const object of objects) {
            await this.client.delete(object);
        }
        if (deleteAllIncludingTypes) {
            console.log('Deleting Type objects');
            const schemas = (await this.client.searchHandles('*:*')).results;
            for (const schema of schemas) {
                await this.client.delete(schema);
            }
        }
    }

    async createOrUpdateTypes(): Promise<string[]> {
        const schemasPath = path.resolve(this.rootDir, 'types');
        const files = await fs.promises.opendir(schemasPath);
        // const promises = []; // COR-384
        const ids = [];
        for await (const file of files) {
            if (file.name.endsWith('.schema.json')) {
                const name = file.name.replace('.schema.json', '');
                await this.createOrUpdateTypeByName(name).catch(console.log);
            } else if (file.name.endsWith('.js')) {
                const name = file.name.replace('.js', '');
                if (fs.existsSync(path.resolve(schemasPath, name + '.schema.json'))) continue;
                const id = await this.createOrUpdateTypeByName(name);
                ids.push(id);
            }
            // promises.push(createOrUpdateType(client, name, json, js)); // Can't do this async, because of COR-384
        }
        return ids;
        // await Promise.all(promises); // COR-384
    }

    async createOrUpdateTypeByName(name: string): Promise<string> {
        const jsonPath = path.resolve(this.rootDir, `./types/${name}.schema.json`);
        let json = '{}';
        if (fs.existsSync(jsonPath)) {
            json = fs.readFileSync(jsonPath, 'utf8');
        }
        const jsPath = path.resolve(this.rootDir, `./types/${name}.js`);
        let js = null;
        if (fs.existsSync(jsPath)) {
            js = fs.readFileSync(jsPath, 'utf8');
        }
        return this.createOrUpdateType(name, json, js);
    }

    async createOrUpdateType(name: string, schema: string, js: string | null, id?: string): Promise<string> {
        console.log(`Processing Type: ${name}`);
        const object: CordraObject = {
            type: 'Schema',
            content: {
                name,
                schema: JSON.parse(schema)
            }
        };
        if (id) object.id = id;
        if (js) object.content.javascript = js;

        const existingObjects = await this.client.search('+type:Schema +/name:' + name);
        // FIXME is doing create when it should update
        for (const existingObject of existingObjects.results) {
            if ((existingObject).content.name === name) {
                object.id = existingObject.id;
                break;
            }
        }
        return this.createOrUpdateObject(object);
    }

    async createOrUpdateObjects(objectFiles: string[]): Promise<string[]> {
        const tasks: Array<() => Promise<void>> = [];
        const ids: string[] = [];
        for (const file of objectFiles) {
            const json = fs.readFileSync(file, 'utf8');
            const objects = JSON.parse(json).results;
            if (!objects) continue;
            for (const object of objects) {
                if (object.payloads) {
                    for (const payload of object.payloads) {
                        const filePath = path.resolve(this.rootDir, './payloads', payload.filename);
                        payload.body = new Blob([fs.readFileSync(filePath)]);
                    }
                }
                tasks.push(async () => {
                    let lastError;
                    for (let i = 0; i < 10; i++) {
                        try {
                            const id = await this.createOrUpdateObject(object);
                            ids.push(id);
                            lastError = undefined;
                            break;
                        } catch (e) {
                            lastError = e;
                            await new Promise(resolve => setTimeout(resolve, 200));
                        }
                    }
                    if (lastError) {
                        console.error("Error: " + object.id);
                        console.error(lastError);
                    }
                });
                // promises.push(createOrUpdateObject(client, object)); // COR-384
            }
        }
        if (process.env.CORDRA_UPDATER_ASYNC) {
            console.log("Updating via pool");
            const pool = new PromisePool(() => {
                const task = tasks.shift();
                if (!task) return undefined;
                return task();
            }, 20);
            await pool.start();
        } else {
            for (const task of tasks) {
                await task();
            }
        }
        return ids;
    }

    async createOrUpdateObject(obj: CordraObject): Promise<string> {
        const exists = obj.id && await this.client.getOrNull(obj.id);
        if (!exists) {
            obj = await this.client.create(obj);
            console.log(`Created object: ${obj.id}`);
        } else {
            obj = await this.client.update(obj);
            console.log(`Updated object: ${obj.id}`);
        }
        return obj.id!;
    }

    private fetchAndSet(file: string, setter: (contents: string) => void): void {
        const filePath = path.resolve(this.rootDir, file);
        if (fs.existsSync(filePath)) {
            const contents = fs.readFileSync(filePath, 'utf8');
            setter(contents);
        }
    }

    async updateDesign(): Promise<void> {
        let found = false;
        let designAddition: DesignBits = {};
        this.fetchAndSet('./design/design.json', contents => {
            designAddition = JSON.parse(contents);
            found = true;
        });
        this.fetchAndSet('./design/design.js', contents => {
            designAddition.javascript = contents;
            found = true;
        });
        this.fetchAndSet('./design/CordraDesign.js', contents => {
            designAddition.builtInTypes = designAddition.builtInTypes || {};
            designAddition.builtInTypes.CordraDesign = designAddition.builtInTypes.CordraDesign || {};
            designAddition.builtInTypes.CordraDesign.javascript = contents;
            found = true;
        });
        this.fetchAndSet('./design/Schema.js', contents => {
            designAddition.builtInTypes = designAddition.builtInTypes || {};
            designAddition.builtInTypes.Schema = designAddition.builtInTypes.Schema || {};
            designAddition.builtInTypes.Schema.javascript = contents;
            found = true;
        });
        this.fetchAndSet('./design/handleMintingConfig.js', contents => {
            designAddition.handleMintingConfig = designAddition.handleMintingConfig || {};
            designAddition.handleMintingConfig.javascript = contents;
            found = true;
        });
        if (!found) {
            console.log('No design found in ' + this.rootDir);
            return;
        }
        const design = await this.client.get('design');
        if (design.content.builtInTypes && designAddition.builtInTypes) {
            Object.assign(design.content.builtInTypes, designAddition.builtInTypes);
            delete designAddition.builtInTypes;
        }
        Object.assign(design.content, designAddition); // FIXME this needs to be a deep copy... or does it?
        if (this.client.baseUri.startsWith("http:")) {
            if (!design.content.allowInsecureAuthentication) {
                console.warn("Turning on allowInsecureAuthentication due to use of http: Cordra baseUri");
            }
            design.content.allowInsecureAuthentication = true;
        }
        await this.client.update(design);
        console.log('Updated design object');
    }

    async loadDefaultTypesAndDesign(): Promise<void> {
        await this.client.update(defaultDesign);
        for (const type of defaultTypes.results) {
            await this.client.create(type);
        }
        console.log('Reset types');
    }
}
