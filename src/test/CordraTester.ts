/* eslint-disable global-require */
import path from "path";
import fs from "fs";
import { Blob, CordraClient, CordraObject } from "@cnri/cordra-client";
import { CordraUpdater } from "../main/CordraUpdater";
import { findPackageRoot } from '../main/util';

let clientInitialized = false;
let packageRoot = findPackageRoot();
let cordra: CordraClient | undefined = undefined;
let objectFiles: string[] = [];

const startupCreatedIds: string[] = [];
const singleTestCreatedIds: string[] = [];

export function setPackageRoot(newRoot: string): void {
    packageRoot = newRoot;
}

export function setObjectFiles(newObjectFiles: string[]): void {
    objectFiles = newObjectFiles;
}

export function getCordraClient(): CordraClient {
    if (cordra) return cordra;
    const configPath = path.resolve(packageRoot, './cordra/client-config.json');
    const config = JSON.parse(fs.readFileSync(configPath, 'utf8'));
    clientInitialized = config.initialized;
    cordra = new CordraClient(config.baseUri, {
        username: config.username,
        password: config.password
    });
    return cordra;
}

async function initializeCordra(): Promise<void> {
    getCordraClient();
    if (!clientInitialized) {
        await initializeCordraFromMain();
        saveInitializedStatusToClientConfig();
    }
    await createTestSchemaObjects(path.resolve(__dirname, 'cordra'));
    await createChaiJsDir();
    await createTestSchemaObjects(path.resolve(packageRoot, 'build', 'test', 'cordra'));
}

async function initializeCordraFromMain() {
    const rootDir = path.resolve(packageRoot, './build/main/cordra');
    const updater = new CordraUpdater(cordra!, rootDir);
    await updater.deleteObjectsIfNeeded(true, true);
    await updater.updateDesign();
    await updater.createOrUpdateTypes();
    const objectFilesAbsolute = objectFiles.map(file => path.resolve(rootDir, file));
    await updater.createOrUpdateObjects(objectFilesAbsolute);
}

function saveInitializedStatusToClientConfig() {
    const configPath = path.resolve('./cordra/client-config.json');
    const config = JSON.parse(fs.readFileSync(configPath, 'utf8'));
    config.initialized = true;
    fs.writeFileSync(configPath, Buffer.from(JSON.stringify(config, null, 2)));
}

async function createChaiJsDir() {
    const jsDirSearch = await cordra!.searchHandles('+type:JsDir');
    if (jsDirSearch.size > 0) {
        console.log('Found JsDir');
        return;
    }
    const filePath = require.resolve('chai/chai.js');
    const body = new Blob([fs.readFileSync(filePath)]);
    let jsDir: CordraObject = {
        type: 'JsDir',
        content: {
            directory: '/node_modules'
        },
        payloads: [
            {
                name: 'chai.js',
                filename: "chai.js",
                mediaType: "application/x-javascript",
                body
            }
        ]
    };
    jsDir = await cordra!.create(jsDir);
    console.log('Created chai JsDir: ' + jsDir.id);
    startupCreatedIds.push(jsDir.id!);
}

async function createTestSchemaObjects(rootDir: string) {
    const updater = new CordraUpdater(cordra!, rootDir);
    await updater.updateDesign();
    const ids = await updater.createOrUpdateTypes();
    startupCreatedIds.push(...ids);
}

async function clearTestObjects(objects: string[]) {
    for (const id of objects) {
        try {
            await cordra!.delete(id);
        } catch (e) {
            console.error(id + ': ' + e.message);
        }
    }
    while (objects.length > 0) {
        objects.pop();
    }
}

export async function runTest(type: string, method: string, params: object): Promise<void> {
    const res = await cordra!.callMethodForType(type, method, params);
    if (res) {
        if (res.testError) throw res;
        if (Object.keys(res).length > 0) console.log(res);
    }
}

export async function createCordraObjectFromResource(filename: string, type: string, id: string): Promise<CordraObject> {
    const jsonPath = path.resolve(`./build/test/resources/objects/${filename}`);
    const content = JSON.parse(fs.readFileSync(jsonPath, 'utf8'));
    let object: CordraObject = {
        id,
        type,
        content
    };
    object = await cordra!.create(object);
    singleTestCreatedIds.push(object.id!);
    return object;
}

export async function createCordraObject(object: CordraObject): Promise<CordraObject> {
    object = await cordra!.create(object);
    singleTestCreatedIds.push(object.id!);
    return object;
}

export function setupJestLifecycle(): void {
    beforeAll(async () => {
        await initializeCordra();
    });

    afterAll(async () => {
        await clearTestObjects(startupCreatedIds);
    });

    afterEach(async () => {
        await clearTestObjects(singleTestCreatedIds);
    });
}
