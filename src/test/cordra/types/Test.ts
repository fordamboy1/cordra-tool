export function catchAndReturnErrors(f: (...args: unknown[]) => unknown) {
    // Return instead of throwing so that errors can be displayed in Jest
    return function (...args: unknown[]): unknown {
        try {
            return f(...args);
        } catch (e) {
            console.error(e);
            e.testError = true;
            Object.defineProperty(e, 'message', {
                enumerable: true
            });
            return e;
        }
    };
}
