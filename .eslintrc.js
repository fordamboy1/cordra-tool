/* eslint-disable prefer-object-spread, quote-props */
const fs = require('fs');
const path = require('path');

let sharedRules = {
    "array-bracket-spacing": "off",
    "arrow-body-style": "off",
    "arrow-parens": "off", // let a hundred flowers bloom
    "camelcase": "off", // allow weird AC1_SingleThing type names. reconsider
    "class-methods-use-this": "off",
    "consistent-return": "warn",
    "curly": "warn",
    "eol-last": "warn",
    "eqeqeq": [ "warn", "smart" ],
    "func-names": "off",
    "function-paren-newline": "off", // reconsider
    "guard-for-in": "off",
    "implicit-arrow-linebreak": "warn",
    "import/extensions": "off",
    "import/order": "warn",
    "import/no-absolute-path": "off", // allow Cordra schema js imports
    "import/no-cycle": "off",
    "import/no-duplicates": "warn",
    "import/no-named-as-default-member": "off",
    "import/no-unresolved": "off",
    "import/no-useless-path-segments": "off",
    "import/prefer-default-export": "off",
    "linebreak-style": "warn",
    "max-classes-per-file": "off",
    "max-len": "off",
    "no-await-in-loop": "off",
    "no-bitwise": "off",
    "no-cond-assign": [ "error", "except-parens" ],
    "no-console": "off",
    "no-constant-condition": [ "warn", { "checkLoops": false } ], // more restrictive than extended configs
    "no-continue": "off",
    "no-else-return": "off",
    "no-lonely-if": "warn",
    "no-mixed-operators": "off",
    "no-multi-spaces": "warn",
    "no-multiple-empty-lines": [ "warn", {
        "max": 2,
        "maxBOF": 0,
        "maxEOF": 0
    } ],
    "no-nested-ternary": "off", // reconsider
    "no-param-reassign": "off",
    "no-path-concat": "off",
    "no-plusplus": "off",
    "no-restricted-syntax": [ "warn", "WithStatement" ],
    "no-trailing-spaces": "warn",
    "no-throw-literal": "off", // Cordra requires throwing strings
    "no-undef-init": "off",
    "no-underscore-dangle": "off",
    "no-unneeded-ternary": "off",
    "nonblock-statement-body-position": "warn",
    "object-curly-newline": [ "warn", { "consistent": true } ],
    "object-curly-spacing": [ "warn", "always" ],
    "object-shorthand": "warn",
    "operator-assignment": "off",
    "operator-linebreak": "off",
    "padded-blocks": "off",
    "prefer-const": [ "warn", {
        "destructuring": "any",
        "ignoreReadBeforeAssign": true
    } ],
    "prefer-destructuring": "off",
    "prefer-object-spread": "off", // reconsider
    "prefer-template": "off",
    "quote-props": [ "warn", "consistent-as-needed" ],
    "radix": "off",
    "space-before-blocks": "warn",
    "space-in-parens": "warn",
    "spaced-comment": "off",
    "yoda": "off"
};

const sharedReactRules = {
    "jsx-a11y/click-events-have-key-events": "off", // reconsider
    "jsx-a11y/label-has-associated-control": "warn",
    "jsx-a11y/no-autofocus": "off", // reconsider
    "jsx-a11y/no-noninteractive-element-interactions": "off", // reconsider
    "jsx-a11y/no-static-element-interactions": "off", // reconsider
    "react/button-has-type": "warn",
    "react/destructuring-assignment": "off",
    "react/jsx-boolean-value": "off",
    "react/jsx-closing-bracket-location": "off",
    "react/jsx-closing-tag-location": "off",
    "react/jsx-curly-brace-presence": "off", // reconsider
    "react/jsx-curly-newline": "off",
    "react/jsx-first-prop-new-line": "off",
    "react/jsx-fragments": "off",
    "react/jsx-indent": [ "warn", 4 ],
    "react/jsx-indent-props": [ "warn", 4 ],
    "react/jsx-max-props-per-line": "off",
    "react/jsx-one-expression-per-line": "off",
    "react/jsx-props-no-multi-spaces": "warn",
    "react/jsx-props-no-spreading": "off",
    "react/jsx-tag-spacing": "warn",
    "react/jsx-wrap-multilines": "off",
    "react/no-access-state-in-setstate": "warn",
    "react/no-array-index-key": "off", // reconsider
    "react/no-did-update-set-state": "off", // reconsider
    "react/no-unescaped-entities": "off", // reconsider
    "react/prefer-stateless-function": "off", // reconsider
    "react/prop-types": "warn",
    "react/require-default-props": "off", // reconsider
    "react/self-closing-comp": "off",
    "react/state-in-constructor": "off",
    "react/sort-comp": "off"
};

const javascriptRules = {
    "comma-dangle": [ "warn", "never" ],
    "dot-notation": "off",
    "indent": [ "warn", 4,
        {
            "MemberExpression": "off",
            "SwitchCase": 1,
            "FunctionDeclaration": { "parameters": 2 },
            "FunctionExpression": { "parameters": 2 },
            "ignoreComments": true,
            "outerIIFEBody": "off"
        }
    ],
    "keyword-spacing": "warn",
    "lines-between-class-members": [ "warn", "always", { "exceptAfterSingleLine": true } ],
    "no-extra-semi": "warn",
    "no-return-await": "warn",
    "no-shadow": [ "warn", { "allow": [ "_", "resolve", "reject" ] } ],
    "no-unused-vars": "warn",
    "no-use-before-define": "off",
    "quotes": "off",
    "semi": "warn",
    "space-before-function-paren": "warn"
};

const types = {
    String: {
        message: 'Use string instead',
        fixWith: 'string'
    },
    Boolean: {
        message: 'Use boolean instead',
        fixWith: 'boolean'
    },
    Number: {
        message: 'Use number instead',
        fixWith: 'number'
    },
    Symbol: {
        message: 'Use symbol instead',
        fixWith: 'symbol'
    },
    Function: {
        message: 'The `Function` type accepts any function-like value; prefer an explicit function type instead'
    },
    // object typing
    Object: {
        message: 'The `Object` type actually means "any non-nullish value", prefer `object`, `{}`, or `unknown`'
    }
    // we are allowing {} and object currently
};

const typescriptRules = {
    "@typescript-eslint/ban-ts-comment": "warn",
    "@typescript-eslint/ban-ts-ignore": "off",
    "@typescript-eslint/ban-types": [ "warn", { types, extendDefaults: false } ],
    "@typescript-eslint/brace-style": "warn",
    "@typescript-eslint/comma-dangle": [ "warn", "never" ],
    "@typescript-eslint/consistent-type-assertions": "warn",
    "@typescript-eslint/explicit-member-accessibility": [ "warn", { "accessibility": "no-public" } ], // more restrictive than extended configs
    "@typescript-eslint/explicit-module-boundary-types": [ "warn", { "allowArgumentsExplicitlyTypedAsAny": true } ],
    "@typescript-eslint/indent": [ "warn", 4,
        {
            "MemberExpression": "off",
            "SwitchCase": 1,
            "FunctionDeclaration": { "parameters": 2 },
            "FunctionExpression": { "parameters": 2 },
            "ignoreComments": true
        }
    ],
    "@typescript-eslint/keyword-spacing": "warn",
    "@typescript-eslint/lines-between-class-members": [ "warn", "always", { "exceptAfterSingleLine": true } ],
    "@typescript-eslint/member-delimiter-style": "warn",
    "@typescript-eslint/naming-convention": "off", // allow names like testAC10_AC13
    "@typescript-eslint/no-empty-interface": "warn",
    "@typescript-eslint/no-extra-semi": "warn",
    "@typescript-eslint/no-floating-promises": "warn",
    "@typescript-eslint/no-inferrable-types": "off",
    "@typescript-eslint/no-misused-promises": [ "warn", { "checksVoidReturn": false } ],
    "@typescript-eslint/no-non-null-assertion": "off",
    "@typescript-eslint/no-shadow": [ "warn", { "allow": [ "_", "resolve", "reject" ] } ],
    "@typescript-eslint/no-throw-literal": "off", // need to throw string errors for Cordra
    "@typescript-eslint/no-unnecessary-type-assertion": "warn",
    "@typescript-eslint/no-unsafe-assignment": "off",
    "@typescript-eslint/no-unsafe-call": "off",
    "@typescript-eslint/no-unsafe-member-access": "off",
    "@typescript-eslint/no-unsafe-return": "off",
    "@typescript-eslint/no-unused-vars": "warn",
    "@typescript-eslint/no-use-before-define": "off",
    "@typescript-eslint/object-curly-spacing": [ "warn", "always" ],
    "@typescript-eslint/quotes": "off",
    "@typescript-eslint/prefer-includes": "warn",
    "@typescript-eslint/prefer-regexp-exec": "off",
    "@typescript-eslint/require-await": "off",
    "@typescript-eslint/restrict-plus-operands": "off", // reconsider in future
    "@typescript-eslint/restrict-template-expressions": [ "warn", {
        "allowNumber": true,
        "allowBoolean": true,
        "allowAny": true,
        "allowNullish": true
    }],
    "no-return-await": "off", // for @typescript-eslint/return-await
    "@typescript-eslint/return-await": [ "warn", "in-try-catch" ], // more restrictive than extended configs
    "@typescript-eslint/semi": "warn",
    "@typescript-eslint/space-before-function-paren": "warn",
    "@typescript-eslint/unbound-method": "off"
};

let sharedExtends = [
    "eslint:recommended",
    "plugin:import/errors",
    "plugin:import/warnings"
];

let javascriptExtends = [
    "airbnb-base"
];

let typescriptExtends = [
    "plugin:import/typescript",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "airbnb-typescript/base"
];

function moduleAvailable(name) {
    try {
        require.resolve(name);
        return true;
    } catch (e) {
        return false;
    }
}

if (moduleAvailable('eslint-plugin-react')) {
    sharedRules = Object.assign({}, sharedRules, sharedReactRules);
    sharedExtends = sharedExtends.concat([
        "plugin:jsx-a11y/recommended",
        "plugin:react/recommended",
        "plugin:react-hooks/recommended"
    ]);
    javascriptExtends = javascriptExtends.concat([
        "airbnb",
        "react-app"
    ]);
    typescriptExtends = typescriptExtends.concat([
        "airbnb-typescript",
        "react-app"
    ]);
}

// find all files named tsconfig.json, *.tsconfig.json, tsconfig.*.json. *.tsconfig.*.json
function findAllTsconfigs(dir, res) {
    if (!dir) dir = '.';
    if (!res) res = [];
    const files = fs.readdirSync(dir);
    for (const file of files) {
        if (file.match(/^(.*\.)?tsconfig(\..*)?\.json$/)) {
            res.push(path.join(dir, file));
        } else if (file === 'node_modules' || file.startsWith('.')) {
            // skip
        } else {
            const newDir = path.join(dir, file);
            if (fs.statSync(newDir).isDirectory()) {
                findAllTsconfigs(newDir, res);
            }
        }
    }
    return res;
}

module.exports = {
    "root": true,
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true,
        "jest": true,
        "node": true,
        "nashorn": true
    },
    "extends": sharedExtends,
    "settings": {
        "import/core-modules": [
            "electron"
        ]
    },
    "overrides": [
        {
            "files": [ "*.js", "*.jsx" ],
            "parser": "@babel/eslint-parser",
            "parserOptions": {
                "ecmaVersion": 2019,
                "sourceType": "module"
            },
            "extends": javascriptExtends,
            "settings": {
                "import/resolver": {
                    "node": {
                        "extensions": [ ".js", ".jsx", ".json", ".ts", ".tsx" ]
                    }
                }
            },
            "rules": Object.assign({}, sharedRules, javascriptRules)
        },
        {
            "files": [ "*.ts", "*.tsx" ],
            "parser": "@typescript-eslint/parser",
            "parserOptions": {
                "project": findAllTsconfigs(),
                "ecmaVersion": 2019,
                "sourceType": "module"
            },
            "plugins": [
                "@typescript-eslint"
            ],
            "extends": typescriptExtends,
            "rules": Object.assign({}, sharedRules, typescriptRules)
        }
    ]
};
