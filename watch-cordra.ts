/* eslint-disable import/no-extraneous-dependencies */
import chokidar from 'chokidar';
import { CordraClient } from '@cnri/cordra-client';
import fs from 'fs';
import path from 'path';
import { CordraUpdater } from './src/main/CordraUpdater';
import { createTypeScriptInterface } from './src/main/InterfaceGenerator';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

let ready = false;
let clientInitialized = false;

const cordra = getCordraClient();
const updater = new CordraUpdater(cordra, 'build/main/cordra');
const watchPaths = ['build/main/cordra/design', 'build/main/cordra/types'];
const watcher = chokidar.watch(watchPaths, {
    ignoreInitial: true,
    ignored: [
        /(^|[/\\])\../, // dotfiles
        '**/*.map',
        '**/*.d.js'
    ],
    persistent: true
});

function getCordraClient() {
    const configPath = path.resolve('./cordra/client-config.json');
    const config = JSON.parse(fs.readFileSync(configPath, 'utf8'));
    clientInitialized = config.initialized;
    return new CordraClient(config.baseUri, {
        username: config.username,
        password: config.password
    });
}

async function createOrUpdate(file: string) {
    if (!ready) return;
    if (file.startsWith('build/main/cordra/design/')) {
        await updater.updateDesign();
    } else {
        const schemaMatches = /([^/]+).schema.json$/.exec(file);
        if (schemaMatches && schemaMatches.length > 1) {
            const name = schemaMatches[1];
            await updater.createOrUpdateTypeByName(name);
            await createTypeScriptInterface(file, 'src/main/cordra/types/interfaces');
            return;
        }
        const jsMatches = /([^/]+).js$/.exec(file);
        if (jsMatches && jsMatches.length > 1) {
            const name = jsMatches[1];
            await updater.createOrUpdateTypeByName(name);
        }
    }
}

async function removeObject(file: string) {
    if (!ready) return;
    console.log(`File ${file} has been removed. Object not deleted from Cordra.`);
}

async function afterReady() {
    if (!clientInitialized) {
        const watched = watcher.getWatched();
        for (const key of Object.keys(watched)) {
            const files = watched[key];
            for (const file of files) {
                await createOrUpdate(file);
            }
        }
    }
    ready = true;
    console.log('Initial scan complete. Ready for changes');
}

watcher
.on('ready', () => afterReady().catch(console.log))
.on('error', error => console.log((`Watcher error: ${JSON.stringify(error)}`)))
.on('add', file => createOrUpdate(file).catch(console.log))
.on('change', file => createOrUpdate(file).catch(console.log))
.on('unlink', file => removeObject(file).catch(console.log));
